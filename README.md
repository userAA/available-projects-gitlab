gitlab page https://gitlab.com/userAA/available-projects-gitlab.git
gitlab comment available-projects-gitlab

1. проект fileupload (загрузчик файлов через сервер cloudinary).
используемые технологии:
    @testing-library/jest-dom,
    @testing-library/react,
    @testing-library/user-event,
    axios,
    react,
    react-dom,
    react-scripts.

2. проект pern-todo (список названий задач, с возможностью их добавления редактирования и удаления).
используемые технологии на фронтенде:
    @testing-library/jest-dom,
    @testing-library/react,
    @testing-library/user-event,
    react,
    react-dom,
    react-scripts.

используемые технологии на бекэнде:
    cors,
    dotenv,
    express,
    nodemon,
    pg,
    pg-hstore,
    sequelize.

3. проект react-hook-use-selector (демонстрация работы хука useSelector).
используемые технологии:
    @testing-library/jest-dom,
    @testing-library/react,
    @testing-library/user-event,
    react,
    react-dom,
    react-redux,
    react-scripts,
    redux.

4. проект react-app (вывод списка потребителей и их денежного бюджета, бюджет можно уменьшать или увеличивать).
используемые технологии:
    @testing-library/jest-dom,
    @testing-library/react,
    @testing-library/user-event,
    react,
    react-dom,
    react-redux,
    react-scripts,
    redux,
    redux-devtools,
    redux-devtools-extension,
    redux-thunk.

5. проект redux-exam (показ картинки по веб-адресу на сервере).
используемые технологии:
    @testing-library/jest-dom,
    @testing-library/react,
    @testing-library/user-event,
    react,
    react-dom,
    react-redux,
    react-scripts,
    redux.

6. проект redux-exam-sagas (показ картинки по веб-адресу на сервере с помощью sagas).
используемые технологии:
    @testing-library/jest-dom,
    @testing-library/react,
    @testing-library/user-event,
    axios,
    react,
    react-dom,
    react-redux,
    react-scripts,
    redux,
    redux-saga.

7. проект redux-exam-thunk (показ картинки по веб-адресу на сервере с помощью thunk).
используемые технологии:
    @testing-library/jest-dom,
    @testing-library/react,
    @testing-library/user-event,
    react,
    react-dom,
    react-redux,
    react-scripts,
    redux,
    redux-thunk.

8. проект react_redux_ts_course (вывод списка имен пользователей и списка названий задач с пагинацией).
используемые технологии:
    @testing-library/jest-dom,
    @testing-library/react,
    @testing-library/user-event,
    @types/jest,
    @types/node,
    @types/react,
    @types/react-dom,
    @types/react-redux,
    axios,
    react,
    react-dom,
    react-redux,
    react-scripts,
    redux,
    redux-thunk,
    typescript.