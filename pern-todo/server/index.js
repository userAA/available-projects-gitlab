require('dotenv').config();
const express = require("express");
//задаем базу данных 
const sequelize = require('./db');
//we defining component cors using which we will send the needed requests from client-application
const cors = require("cors");

//вытаскиваем модель базы данных названий заданий
const {Todo} = require('./model');

const PORT = process.env.PORT || 5000;

const app = express();
app.use(cors());
app.use(express.json());

//МАРШРУТИЗАТОРЫ//

//маршрутизатор запроса добавления в базу данных наваний заданий название нового задания
app.post("/todos", async(req,res) => 
{
    try 
    {
        //определяем само название нового задания
        const {description} = req.body;
        //производим указанное добавление
        const newTodo = Todo.create({description: description});
        res.json("SUCCESS");
    } 
    catch (err) 
    {
        console.error(err.message);
    }
})

//маршрутизатор запроса на получение всех названий заданий из соответствующей базы данных
app.get("/todos", async (req,res) => 
{
    try
    {
        //определяем все названия заданий в базе данных Todo 
        const allTodos = await Todo.findAll();
        //отправляем полученный результат на клиент приложение
        res.json({allTodos: allTodos});
    } 
    catch (err) 
    {
        console.error(err.message);
    }
})

//маршрутизатор запроса на изменение названия задания в соответствующей базе данных с определенным идентификатором
app.put("/todos/:id", async (req, res) => 
{
    try 
    {
        //получаем идентификатор изменяемого названия задания 
        const {id} = req.params;
        //получаем окончательный вариант названия изменяемого названия
        const {description} = req.body;
        //производим указанное изменение
        await Todo.update({description: description}, {where: {id: id}});
        res.json("Todo was updated!")
    } 
    catch (err) 
    {
        console.error(err.message);
    }
})

//маршрутизатор запроса на удаление названия задания с заданным идентификатором из соответствующей базы данных
app.delete("/todos/:id", async (req, res) => 
{
    try 
    {
        //определяем идентификатор удаляемого названия задания
        const {id} = req.params;
        //производим указанное удаление
        await Todo.destroy({
            where: {
                id: id
            }
        })
        res.json("Todo was deleted!");
    } 
    catch (err) 
    {
        console.log(err.message);
    }
})

//функция подключения к базе данных postgress и запуска сервера
const start = async () => {
    try 
    {
        //запускаем базу данных postgress
        await sequelize.authenticate();
        await sequelize.sync();
        //прослушиваем сервер
        app.listen(PORT, () => console.log(`Server started on port ${PORT}`));
    }
    catch (e) 
    {
        console.log(e);
    }
} 

//активируем функцию подключения к базе данных postgress и запуска сервера
start();