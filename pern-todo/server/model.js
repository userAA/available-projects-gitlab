const sequelize = require('./db');
const {DataTypes} = require('sequelize');

//модель базы данных названий заданий
const Todo = sequelize.define('todo', 
{
    //идентификатор названия задания
    id : {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
    //само название задания
    description: {type: DataTypes.STRING}
})

//экспортируем модель базы данных названий заданий
module.exports = 
{
    Todo
}