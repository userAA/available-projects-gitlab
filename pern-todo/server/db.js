const {Sequelize} = require("sequelize");

module.exports = new Sequelize(
    //Database name
    process.env.DB_NAME, 
    //User
    process.env.DB_USER,
    //PASSWORD
    process.env.DB_PASSWORD, 
    {
        dialect: 'postgres',
        host: process.env.DB_HOST,
        port: process.env.DB_PORT
    }
)