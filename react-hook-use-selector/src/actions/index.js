//вытаскиваем функции действий, связанных с счетчиком
import counterActions from "./counterActions";
//вытаскиваем функции действий, связанных с текущим пользователем
import userActions from "./userActions";

//экспортируем все действия
const allActions = 
{
    counterActions,
    userActions
}

export default allActions;